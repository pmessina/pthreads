#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

typedef struct AlarmTag {
    int seconds;
    char message[64];
} alarm_t;

void* alarmThread(void* arg) {
    alarm_t* alarm = (alarm_t*)arg;
    int status;

    status = pthread_detach(pthread_self());
    if (status != 0)
        fprintf(stderr, "This should be err_abort, Detach thread");
    sleep(alarm->seconds);
    printf("(%d) %s\n", alarm->seconds, alarm->message);
    free(alarm);
    return NULL;
}

int main() {

    int status;
    char line[128];
    alarm_t* alarm;
    pthread_t thread;

    while (1) {
        printf("Alarm> ");
        if (fgets(line, sizeof(line), stdin) == NULL) exit(0);
        if (strlen(line) <= 1) continue;
        alarm = (alarm_t*)malloc(sizeof (alarm_t));
        if (alarm == NULL)
            fprintf(stderr, "This should be err_abort, Allocate alarm");

        /* Parse input line into seconds (%d) and a message
        * (%64[^\n]), consisting of up to 64 characters seperated from
        * the seconds by whitespace.
        */
        if (sscanf(line, "%d %64[^\n]", &alarm->seconds, alarm->message) < 2) {
            fprintf(stderr, "Bad command\n");
            free(alarm);
        }
        else {
            status = pthread_create(&thread, NULL, alarmThread, alarm);
            if (status != 0)
                fprintf(stderr, 
                        "This should be err_abort, Create alarm thread");
        }

    }
    
    return 0;
}
